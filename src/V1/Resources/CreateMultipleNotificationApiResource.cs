﻿using System.Collections.Generic;

namespace ProSkive.Clients.Notifications.V1.Resources
{
    public class CreateMultipleNotificationApiResource
    {
        public List<string> Listeners { get; set; }
        public string Message { get; set; }  
        public string Link { get; set; }
        public string Tag { get; set; }
    }
}