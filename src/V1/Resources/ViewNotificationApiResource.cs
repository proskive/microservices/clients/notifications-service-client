﻿using System;

namespace ProSkive.Clients.Notifications.V1.Resources
{
    public class ViewNotificationApiResource
    {
        public string Id { get; set; }
        public string Listener { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public bool Dismissed { get; set; }
        public string Tag { get; set; }
        public DateTime Date { get; set; }
    }
}