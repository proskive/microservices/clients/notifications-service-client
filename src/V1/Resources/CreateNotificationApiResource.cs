﻿namespace ProSkive.Clients.Notifications.V1.Resources
{
    public class CreateNotificationApiResource
    {
        public string Listener { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public string Tag { get; set; }
    }
}