﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using ProSkive.Clients.Notifications.V1.Resources;

namespace ProSkive.Clients.Notifications.V1
{
    public class NotificationsFlurlClient : INotificationsClient
    {
        public string BaseAddress { get; }
        
        public NotificationsFlurlClient(string baseAddress)
        {
            BaseAddress = baseAddress;
        }

        /* ********
         * Create
         * *********/
        public async Task<HttpResponseMessage> CreateNotificationWithResponseAsync(CreateNotificationApiResource resource, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment("api/v1/notifications");

            return await CreateRequestWithToken(url, token)
                .PostJsonAsync(resource);
        }

        public async Task<ViewNotificationApiResource> CreateNotificationAsync(CreateNotificationApiResource resource, string token = null)
        {
            return await CreateNotificationWithResponseAsync(resource, token)
                .ReceiveJson<ViewNotificationApiResource>();
        }

        /* ********
         * Create (Multiple)
         * *********/
        public async Task<HttpResponseMessage> CreateMultipleNotificationsWithResponseAsync(CreateMultipleNotificationApiResource resource, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment("api/v1/notifications/multiple");

            return await CreateRequestWithToken(url, token)
                .PostJsonAsync(resource);
        }

        public async Task<List<ViewNotificationApiResource>> CreateMultipleNotificationsAsync(CreateMultipleNotificationApiResource resource, string token = null)
        {
            return await CreateMultipleNotificationsWithResponseAsync(resource, token)
                .ReceiveJson<List<ViewNotificationApiResource>>();
        }

        /* ********
         * Get All
         * *********/
        public async Task<HttpResponseMessage> GetAllNotificationsWithResponseAsync(bool? dismissed = null, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment("api/v1/notifications")
                .SetQueryParam("dismissed", dismissed);

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }

        public async Task<List<ViewNotificationApiResource>> GetAllNotificationsAsync(bool? dismissed = null, string token = null)
        {
            return await GetAllNotificationsWithResponseAsync(dismissed, token)
                .ReceiveJson<List<ViewNotificationApiResource>>();
        }
        
        /* ********
         * Get All By Listener
         * *********/

        public async Task<HttpResponseMessage> GetAllNotificationsByListenerWithResponseAsync(string listener, bool? dismissed = null, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/notifications/listener/{listener}")
                .SetQueryParam("dismissed", dismissed);

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }
        
        public async Task<List<ViewNotificationApiResource>> GetAllNotificationsByListenerAsync(string listener, bool? dismissed = null, string token = null)
        {
            return await GetAllNotificationsByListenerWithResponseAsync(listener, dismissed, token)
                .ReceiveJson<List<ViewNotificationApiResource>>();
        }
        
        /* ********
         * Get All By Tag
         * *********/

        public async Task<HttpResponseMessage> GetAllNotificationsByTagWithResponseAsync(string tag, bool? dismissed = null, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/notifications/tag/{tag}")
                .SetQueryParam("dismissed", dismissed);

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }
        
        public async Task<List<ViewNotificationApiResource>> GetAllNotificationsByTagAsync(string tag, bool? dismissed = null, string token = null)
        {
            return await GetAllNotificationsByTagWithResponseAsync(tag, dismissed, token)
                .ReceiveJson<List<ViewNotificationApiResource>>();
        }
        
        /* ********
         * Get All By Listener and Tag
         * *********/

        public async Task<HttpResponseMessage> GetAllNotificationsByListenerAndTagWithResponseAsync(string listener, string tag, bool? dismissed = null, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/notifications/listener/{listener}/tag/{tag}")
                .SetQueryParam("dismissed", dismissed);

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }
        
        public async Task<List<ViewNotificationApiResource>> GetAllNotificationsByListenerAndTagAsync(string listener, string tag, bool? dismissed = null, string token = null)
        {
            return await GetAllNotificationsByListenerAndTagWithResponseAsync(listener, tag, dismissed, token)
                .ReceiveJson<List<ViewNotificationApiResource>>();
        }

        /* ********
         * Get
         * *********/
        
        public async Task<HttpResponseMessage> GetNotificationWithResponseAsync(string id, bool? dismissed = null, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/notifications/{id}");

            return await CreateRequestWithToken(url, token)
                .GetAsync();
        }

        public async Task<ViewNotificationApiResource> GetNotificationAsync(string id, bool? dismissed = null, string token = null)
        {
            return await GetNotificationWithResponseAsync(id, dismissed, token)
                .ReceiveJson<ViewNotificationApiResource>();
        }
        
        /* ********
         * Dismiss
         * *********/

        public async Task<HttpResponseMessage> DismissNotificationWithResponseAsync(string id, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/notifications/{id}/dismiss");

            return await CreateRequestWithToken(url, token)
                .PatchAsync(null);
        }

        public async Task<ViewNotificationApiResource> DismissNotificationAsync(string id, string token = null)
        {
            return await DismissNotificationWithResponseAsync(id, token)
                .ReceiveJson<ViewNotificationApiResource>();
        }
        
        /* ********
         * UndoDismiss
         * *********/

        public async Task<HttpResponseMessage> UndoDismissNotificationWithResponseAsync(string id, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/notifications/{id}/dismiss/undo");

            return await CreateRequestWithToken(url, token)
                .PatchAsync(null);
        }

        public async Task<ViewNotificationApiResource> UndoDismissNotificationAsync(string id, string token = null)
        {
            return await UndoDismissNotificationWithResponseAsync(id, token)
                .ReceiveJson<ViewNotificationApiResource>();
        }
        
        /* ********
         * Delete
         * *********/

        public async Task<HttpResponseMessage> DeleteNotificationWithResponseAsync(string id, string token = null)
        {
            var url = BaseAddress
                .AppendPathSegment($"api/v1/notifications/{id}");

            return await CreateRequestWithToken(url, token)
                .DeleteAsync();
        }
        
        private IFlurlRequest CreateRequestWithToken(Url url, string token)
        {
            return string.IsNullOrWhiteSpace(token) ? new FlurlRequest(url) : url.WithOAuthBearerToken(token);
        }
    }
}