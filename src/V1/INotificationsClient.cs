﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ProSkive.Clients.Notifications.V1.Resources;

namespace ProSkive.Clients.Notifications.V1
{
    public interface INotificationsClient
    {
        Task<HttpResponseMessage> CreateNotificationWithResponseAsync(CreateNotificationApiResource resource, string token = null);
        Task<ViewNotificationApiResource> CreateNotificationAsync(CreateNotificationApiResource resource, string token = null);
        Task<HttpResponseMessage> CreateMultipleNotificationsWithResponseAsync(CreateMultipleNotificationApiResource resource, string token = null);
        Task<List<ViewNotificationApiResource>> CreateMultipleNotificationsAsync(CreateMultipleNotificationApiResource resource, string token = null);
        Task<HttpResponseMessage> GetAllNotificationsWithResponseAsync(bool? dismissed = null, string token = null);
        Task<List<ViewNotificationApiResource>> GetAllNotificationsAsync(bool? dismissed = null, string token = null);
        Task<HttpResponseMessage> GetAllNotificationsByListenerWithResponseAsync(string listener, bool? dismissed = null, string token = null);
        Task<List<ViewNotificationApiResource>> GetAllNotificationsByListenerAsync(string listener, bool? dismissed = null, string token = null);
        Task<HttpResponseMessage> GetAllNotificationsByTagWithResponseAsync(string tag, bool? dismissed = null, string token = null);
        Task<List<ViewNotificationApiResource>> GetAllNotificationsByTagAsync(string tag, bool? dismissed = null, string token = null);
        Task<HttpResponseMessage> GetAllNotificationsByListenerAndTagWithResponseAsync(string listener, string tag, bool? dismissed = null, string token = null);
        Task<List<ViewNotificationApiResource>> GetAllNotificationsByListenerAndTagAsync(string listener, string tag, bool? dismissed = null, string token = null);
        Task<HttpResponseMessage> GetNotificationWithResponseAsync(string id, bool? dismissed = null, string token = null);
        Task<ViewNotificationApiResource> GetNotificationAsync(string id, bool? dismissed = null, string token = null);        
        Task<HttpResponseMessage> DismissNotificationWithResponseAsync(string id, string token = null);
        Task<ViewNotificationApiResource> DismissNotificationAsync(string id, string token = null);
        Task<HttpResponseMessage> UndoDismissNotificationWithResponseAsync(string id, string token = null);
        Task<ViewNotificationApiResource> UndoDismissNotificationAsync(string id, string token = null);
        Task<HttpResponseMessage> DeleteNotificationWithResponseAsync(string id, string token = null);
    }
}